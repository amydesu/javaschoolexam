package com.tsystems.javaschool.tasks.calculator;


import java.util.*;

public class Calculator {

    private List<Character> BRACKETS = Arrays.asList('(', ')');
    private List<Character> OPERATORS = Arrays.asList('+', '-', '*', '/');
    private char DELIMITER = '.';

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null){
            return null;
        }
        StringBuilder fullStatement = new StringBuilder((statement));
        StringBuilder result = evaluateInBrackets(fullStatement);
        if(result == null){
            return null;
        }
        double res = Double.parseDouble(result.toString());
        if(Double.isInfinite(res) || Double.isNaN(res)){
            return null;
        }
        if(res % 1 == 0){
            return String.valueOf((int)res);
        }
        if((res * 10000) % 1 == 0){
            return String.valueOf(res);
        }
        return String.format("%.4f", res);
    }

    private StringBuilder evaluateInBrackets(StringBuilder block) {
        if(block.length() == 0){
            return null;
        }
        Deque<Character> lastBracket = new ArrayDeque<>();
        List<Double> numbers = new ArrayList<>();
        List<Character> lastOperators = new ArrayList<>();
        int lastExpressionIndex = -1;
        for (int i = 0; i < block.length(); i++) {
            char symbol = block.charAt(i);
            try{
                if((symbol == DELIMITER &&
                        (!Character.isDigit(block.charAt(i - 1)) || !Character.isDigit(block.charAt(i + 1)))) ||
                (OPERATORS.contains(symbol) &&
                        ( (!Character.isDigit(block.charAt(i - 1)) && block.charAt(i - 1) != BRACKETS.get(1)) ||
                                (!Character.isDigit(block.charAt(i + 1)) && block.charAt(i + 1) != BRACKETS.get(0)) ))){
                    return null;
                }
                if(symbol == BRACKETS.get(0)){
                    if(i == 0)
                    {
                        if(block.charAt(i + 1) != BRACKETS.get(0) && !Character.isDigit(block.charAt(i + 1))){
                            return null;
                        }
                    }
                    else if((block.charAt(i - 1) != BRACKETS.get(0) && !OPERATORS.contains(block.charAt(i - 1))) ||
                            (block.charAt(i + 1) != BRACKETS.get(0) && !Character.isDigit(block.charAt(i + 1)))){
                        return null;
                    }
                }
                if(symbol == BRACKETS.get(1)){
                    if(i == block.length() - 1){
                        if(block.charAt(i - 1) != BRACKETS.get(0) && !Character.isDigit(block.charAt(i - 1))){
                            return null;
                        }
                    }
                    else if((block.charAt(i + 1) != BRACKETS.get(0) && !OPERATORS.contains(block.charAt(i + 1))) ||
                           (block.charAt(i - 1) != BRACKETS.get(0) && !Character.isDigit(block.charAt(i - 1)))){
                        return null;
                    }
                }
            }
            catch(IndexOutOfBoundsException e){
                return null;
            }
            if(BRACKETS.contains(symbol)){
                if(BRACKETS.indexOf(symbol) == 0){
                    lastBracket.addFirst(symbol);
                }
                else{
                    if(BRACKETS.indexOf(lastBracket.peekFirst()) != 0){
                        return null;
                    }
                    lastBracket.pollFirst();
                }
            }
            if((OPERATORS.contains(symbol) || i == block.length() - 1) && lastBracket.size() == 0){
                StringBuilder expression;
                if(i == block.length() - 1){
                    expression = new StringBuilder(block.subSequence(lastExpressionIndex + 1, i + 1));
                }
                else{
                   expression = new StringBuilder(block.subSequence(lastExpressionIndex + 1, i));
                }
                lastExpressionIndex = i;
                double number;
                boolean hasBrackets = false;
                char[] chars = expression.toString().toCharArray();
                for (char element : chars) {
                    if(element == BRACKETS.get(0) || element == BRACKETS.get(1)){
                        hasBrackets = true;
                        break;
                    }
                }
                try{
                    if(hasBrackets){
                        expression.delete(0, 1);
                        expression.delete(expression.length() - 1, expression.length());
                        number = Double.parseDouble(evaluateInBrackets(expression).toString());
                    }
                    else{
                        number = Double.parseDouble(expression.toString());
                    }
                }
                catch(NumberFormatException e){
                    return null;
                }
                if(lastOperators.isEmpty() || (lastOperators.get(lastOperators.size() - 1) == OPERATORS.get(0) ||
                        lastOperators.get(lastOperators.size() - 1) == OPERATORS.get(1))){
                    if(i != block.length() - 1){
                        lastOperators.add(symbol);
                    }
                    numbers.add(number);
                }
                else{
                    numbers.set(numbers.size() - 1, count(numbers.get(numbers.size() - 1),
                            number, lastOperators.get(lastOperators.size() - 1)));
                    lastOperators.remove(lastOperators.size() - 1);
                    if(i != block.length() - 1){
                        lastOperators.add(symbol);
                    }
                }
            }
        }
        if(lastBracket.size() != 0){
            return null;
        }
        double result = numbers.get(0);
        for (int i = 0; i < lastOperators.size(); i++) {
            result = count(result, numbers.get(i + 1), lastOperators.get(i));
        }
        return new StringBuilder(String.valueOf(result));
    }

    private double count(double first, double second, char operator){
        switch (operator){
            case '+':
                return first + second;

            case '-':
                return first - second;

            case '*':
                return first * second;

            case '/':
                return  first / second;

            default:
                throw new IllegalArgumentException("Wrong operator");
        }
    }
}
