package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        List<String> lines = new ArrayList<>();
        if(sourceFile == null || targetFile == null){
            throw new IllegalArgumentException();
        }
        if(!sourceFile.exists()){
            return false;
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
             PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(targetFile, true)))){
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                lines.add(nextLine);
            }
            Collections.sort(lines);
            List<Integer> duplicates = new ArrayList<>();
            ListIterator<String> iterator= lines.listIterator();
            while(iterator.hasNext()){
                if(iterator.hasPrevious()){
                    String previous = iterator.previous();
                    iterator.next();
                    String next = iterator.next();
                    if(!iterator.hasPrevious() || !previous.equals(next)){
                        duplicates.add(1);
                    }
                    else{
                        duplicates.set(duplicates.size() - 1, duplicates.get(duplicates.size() - 1) + 1);
                        iterator.remove();
                    }
                }
                else{
                    iterator.next();
                    duplicates.add(1);
                }
            }
            targetFile.createNewFile();
            for (int i = 0; i < lines.size(); i++) {
                writer.println(lines.get(i) + "[" + String.valueOf(duplicates.get(i)) + "]");
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return true;
    }
}
